package com.mangelgarciaroig.simplehadoopmocklibrary.mapreduce;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.log4j.Logger;

public class WordCountReducer extends Reducer<Text, LongWritable, Text, LongWritable> 
{
	private LongWritable sum = new LongWritable(0);
	
	public void reduce(Text key, Iterable<LongWritable> values, Context context)
	{		
		long occurences = 0;
		
		for (LongWritable partialOccurence : values){
			occurences+= partialOccurence.get();
		}
		
		sum.set(occurences);
				
		try {
			context.write(key, sum);
		} catch (IOException | InterruptedException e) {
			Logger.getLogger(this.getClass()).error("Error reducing data", e);
		}		
	}
}