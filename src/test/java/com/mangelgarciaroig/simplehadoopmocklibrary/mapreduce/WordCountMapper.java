package com.mangelgarciaroig.simplehadoopmocklibrary.mapreduce;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

public class WordCountMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

	private Text reduceKey = new Text();
	private LongWritable reduceValue = new LongWritable(1);
	
	@Override
	public void map(LongWritable key, Text value, Context context)
	{
		StringTokenizer st = new StringTokenizer(value.toString());
		while (st.hasMoreTokens()){
			reduceKey.set(st.nextToken());
			try {
				context.write(reduceKey, reduceValue);
			} catch (IOException | InterruptedException e) {
				Logger.getLogger(this.getClass()).error("Error outputting data for the reduccer", e);
			}
		}
	}
}
