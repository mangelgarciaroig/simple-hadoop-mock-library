package com.mangelgarciaroig.simplehadoopmocklibrary;

import java.util.Iterator;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.junit.Before;
import org.junit.Test;
import org.apache.hadoop.mapreduce.Mapper.Context;

import com.mangelgarciaroig.simplehadoopmocklibrary.mapreduce.WordCountMapper;
import com.mangelgarciaroig.simplehadoopmocklibrary.KeyValuedStoreBuilder;
import com.mangelgarciaroig.simplehadoopmocklibrary.MockedContextBuilder;
import com.mangelgarciaroig.simplehadoopmocklibrary.KeyValuedStore;

import static org.junit.Assert.*;

public class WordCountMapperTest 
{		
	private WordCountMapper mapper;
	private KeyValuedStore<Text, LongWritable> mockedOutputBuffer;
	private Context mockedContext;
	
	@Before
	public void setUp()
	{		
		mockedOutputBuffer = new KeyValuedStore<Text, LongWritable>();
		MockedContextBuilder<Text, LongWritable> builder = new MockedContextBuilder<Text, LongWritable>(mockedOutputBuffer);		
		
		mockedContext = builder.buildMapperContext();
		mapper = new WordCountMapper();
	}
	
	@Test
	public void testMapper()
	{									
		final String aWord = "dog";
		final String anotherWord = "cat";
		final String lineInputToBeProcessed = aWord + " " + anotherWord + " " + aWord;
		
		final int numOfFirstWordItems = 2;
		final int expectedFirstWordCount = 2;
		
		final int numOfSecondWordItems = 1;
		final int expectedSecondWordCount = 1;
		
		final int totalOutputItemsCount = numOfFirstWordItems + numOfSecondWordItems;
		final int lineNumber = 0;						
		
		mapper.map(new LongWritable(lineNumber), new Text(lineInputToBeProcessed), mockedContext);
		
		assertEquals("Unexpected mapper output size", totalOutputItemsCount, mockedOutputBuffer.size());				
		
		checkWordRelatedResult(aWord, mockedOutputBuffer, numOfFirstWordItems, expectedFirstWordCount);
		checkWordRelatedResult(anotherWord, mockedOutputBuffer, numOfSecondWordItems, expectedSecondWordCount);				
	}
	
	@Test
	public void testMapperSimpified()
	{
		final String aWord = "dog";
		final String anotherWord = "cat";
		final String lineInputToBeProcessed = aWord + " " + anotherWord + " " + aWord;
		final int lineNumber = 0;
			
		mapper.map(new LongWritable(lineNumber), new Text(lineInputToBeProcessed), mockedContext);
		
		KeyValuedStoreBuilder<Text, LongWritable> expectedStoreBuilder = new KeyValuedStoreBuilder<Text, LongWritable>();
		expectedStoreBuilder.addItem(new Text(aWord), new LongWritable(1));
		expectedStoreBuilder.addItem(new Text(anotherWord), new LongWritable(1));
		expectedStoreBuilder.addItem(new Text(aWord), new LongWritable(1));
		KeyValuedStore<Text, LongWritable> expectedOutput =  expectedStoreBuilder.build();
		
		assertEquals("The word count mapper output was not the expected", expectedOutput, mockedOutputBuffer);
	}
	
	private void checkWordRelatedResult(String word, KeyValuedStore<Text, LongWritable> output, int expectedOutputSize, long expectedCount)
	{		
		final Text searchedKey = new Text(word);
		
		final int outputByKeySize = output.countElementsByKey(searchedKey);
		final boolean outputForThisKeyHasTheExpectedSize = (outputByKeySize == expectedOutputSize);
		
		assertTrue("Unexpected word count output length for key", outputForThisKeyHasTheExpectedSize);
		if (!outputForThisKeyHasTheExpectedSize) return;
		
		long countFoundInOutput = 0;
		Iterator<LongWritable> keyMapperOutputIterator = output.filterOutputByKey(searchedKey);
		while (keyMapperOutputIterator.hasNext()){
			LongWritable keyMapperOutput = keyMapperOutputIterator.next();
			countFoundInOutput += keyMapperOutput.get();			
		}							
		
		assertEquals("Unexpected sum value for key in word count mapper output", expectedCount, countFoundInOutput);
	}
}
