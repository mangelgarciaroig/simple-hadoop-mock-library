package com.mangelgarciaroig.simplehadoopmocklibrary;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.junit.Test;

import com.mangelgarciaroig.simplehadoopmocklibrary.KeyValuedStore;
import com.mangelgarciaroig.simplehadoopmocklibrary.MockedJobBuilder;
import com.mangelgarciaroig.simplehadoopmocklibrary.mapreduce.WordCountMapper;
import com.mangelgarciaroig.simplehadoopmocklibrary.mapreduce.WordCountReducer;

import static org.junit.Assert.*;

public class WordCountJobTest 
{
	@Test
	public void testJob() 
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException, ClassNotFoundException, InterruptedException
	{
		final String word1 = "aa";
		final String word2 = "cat";
		final String word3 = "dog";
		final long frequencyWord1 = 1;
		final long frequencyWord2 = 2;
		final long frequencyWord3 = 3;
		
		MockedJobBuilder<LongWritable, Text, Text, LongWritable, Text, LongWritable> mockedJobBuilder 
			= new MockedJobBuilder<LongWritable, Text, Text, LongWritable, Text, LongWritable>();					
		
		WordCountMapper mapper = new WordCountMapper();
		WordCountReducer reducer = new WordCountReducer();
		
		final String firstInputLine = String.format("%s %s %s", word3, word2, word3);
		final String secondInputLine = String.format("%s %s %s", word1, word2, word3);
		
		KeyValuedStore<LongWritable, Text> input = buildJobInput(firstInputLine, secondInputLine);		
		KeyValuedStore<Text, LongWritable> output = new KeyValuedStore<Text, LongWritable>();
				
		Job job = mockedJobBuilder.build(mapper, reducer, input, output);						
		job.waitForCompletion(true);
		
		KeyValuedStore<Text, LongWritable> expectedOutput = new KeyValuedStore<Text, LongWritable>();
		expectedOutput.append(new Text(word1), new LongWritable(frequencyWord1));
		expectedOutput.append(new Text(word2), new LongWritable(frequencyWord2));
		expectedOutput.append(new Text(word3), new LongWritable(frequencyWord3));
		
		assertEquals("Unexpected job output", expectedOutput, output);			
	}
	
	private KeyValuedStore<LongWritable, Text> buildJobInput (String... inputLines){
		KeyValuedStore<LongWritable, Text> input = new KeyValuedStore<LongWritable, Text>();
		
		long lineNumber = 0;
		for (String currentInputLine : inputLines){
			input.append(new LongWritable(lineNumber++), new Text(currentInputLine));
		}
		
		return input;
	}
}
