package com.mangelgarciaroig.simplehadoopmocklibrary;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.junit.Before;
import org.junit.Test;

import com.mangelgarciaroig.simplehadoopmocklibrary.mapreduce.WordCountReducer;


public class WordCountReducerTest 
{
	private WordCountReducer reducer;
	private KeyValuedStore<Text, LongWritable> mockedOutputBuffer;
	private Context mockedContext;
	
	@Before
	public void setUp()
	{		
		mockedOutputBuffer = new KeyValuedStore<Text, LongWritable>();
		MockedContextBuilder<Text, LongWritable> builder = new MockedContextBuilder<Text, LongWritable>(mockedOutputBuffer);		
		
		mockedContext = builder.buildReducerContext();
		reducer = new WordCountReducer();
	}
	
	@Test
	public void testReducer()
	{	
		final String aWord = "perro";	
		final long expectedWordCount = 3;
		
		List<LongWritable> valuesToBeReduced = new ArrayList<LongWritable>();
		valuesToBeReduced.add(new LongWritable(1));		
		valuesToBeReduced.add(new LongWritable(2));
		
		reducer.reduce(new Text(aWord), valuesToBeReduced, mockedContext);
		
		final int expectedReducedOutputSize = 1;
		final int obtainedReducedOutputSize = mockedOutputBuffer.size();
		
		KeyValuedStoreBuilder<Text, LongWritable> expectedStoreBuilder = new KeyValuedStoreBuilder<Text, LongWritable>();
		expectedStoreBuilder.addItem(new Text(aWord), new LongWritable(expectedWordCount));		
		KeyValuedStore<Text, LongWritable> expectedOutput =  expectedStoreBuilder.build();
				
		assertEquals("Unexpected word count reducer output size", expectedReducedOutputSize, obtainedReducedOutputSize);									
		assertEquals("Unexpected word count reducer output content", expectedOutput, mockedOutputBuffer);
	}
}
