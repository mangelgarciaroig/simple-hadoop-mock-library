package com.mangelgarciaroig.simplehadoopmocklibrary;

import static org.mockito.Mockito.mock;

import com.rits.cloning.Cloner;

public class MockedContextBuilder<KeyType, ValueType>
{			
	private final MockedHadoopContextWriteMethod<KeyType, ValueType> hadoopContextWriteMethodMocker;
	private Cloner cloner = new Cloner();
	
	public MockedContextBuilder(final KeyValuedStore<KeyType, ValueType> mapperOutputBuffer)
	{
		this.hadoopContextWriteMethodMocker = new MockedHadoopContextWriteMethod<KeyType, ValueType>(mapperOutputBuffer, cloner);
	}
			
	public org.apache.hadoop.mapreduce.Mapper.Context buildMapperContext()
	{			
		return (org.apache.hadoop.mapreduce.Mapper.Context) buildContext(org.apache.hadoop.mapreduce.Mapper.Context.class);		
	}
	
	public org.apache.hadoop.mapreduce.Reducer.Context buildReducerContext()
	{		
		return (org.apache.hadoop.mapreduce.Reducer.Context) buildContext(org.apache.hadoop.mapreduce.Reducer.Context.class);		
	}
	
	private Object buildContext(Class mockedContextObjectTargetClass)
	{			
		Object mockedContext = mock(mockedContextObjectTargetClass, hadoopContextWriteMethodMocker);
		
		return mockedContext;
	}
}
