package com.mangelgarciaroig.simplehadoopmocklibrary;

import java.util.Iterator;
import java.util.List;

public class FilteredMockedOutputBufferIterator<KeyType,ValueType> implements Iterator<ValueType>
{
	private List<KeyValuedBufferItem<KeyType,ValueType>> bufferedOutputContent;
	private KeyType searchedKey;
	
	FilteredMockedOutputBufferIterator(List<KeyValuedBufferItem<KeyType,ValueType>> bufferedOutputContent, KeyType searchedKey)
	{
		this.bufferedOutputContent = bufferedOutputContent;
		this.searchedKey = searchedKey;
	}

	private int currentPosition = -1;

	@Override
	public boolean hasNext() {	
		int nextPosition = nextElementPosition();
		
		if (nextPosition > currentPosition){
			return true;
		}
		
		return false;
	}

	@Override
	public ValueType next() {
		final boolean noMoreValuesAvailable = !hasNext();
		if (noMoreValuesAvailable) return null;
		
		final int nextPosition = nextElementPosition();
		ValueType next = bufferedOutputContent.get(nextPosition).getValue();					
		currentPosition = nextPosition;
		
		return next;
	}

	@Override
	public void remove() {				
		throw new UnsupportedOperationException(this.getClass().getName() + ".remove not supported");
	}
	
	private int nextElementPosition()
	{													
		for (int currentIndex = currentPosition + 1; currentIndex < bufferedOutputContent.size(); currentIndex++){
			if (isKeyTypeAtElementAtPositionCompatible(currentIndex)) return currentIndex;
		}
		
		return -1;
	}
	
	private boolean isKeyTypeAtElementAtPositionCompatible(int position){
		KeyValuedBufferItem<KeyType,ValueType> currentOutputItem = bufferedOutputContent.get(position);
		
		if (currentOutputItem.hasTheSameKeyOf(searchedKey)){
			return true;
		}
		
		return false;
	}
	
}
