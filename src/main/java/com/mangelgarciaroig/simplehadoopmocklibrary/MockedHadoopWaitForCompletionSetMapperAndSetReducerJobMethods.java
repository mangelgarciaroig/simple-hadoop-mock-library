package com.mangelgarciaroig.simplehadoopmocklibrary;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

class MockedHadoopWaitForCompletionSetMapperAndSetReducerJobMethods
	<MapperKeyType,MapperValueType,ReducerKeyType,ReducerValueType,OutputKeyType,OutputValueType> 
	extends MockedHadoopGenericMethod
{
	private final static String WAITFORCOMPLETION_METHOD_NAME = "waitForCompletion";
	private final static String SETMAPPERCLASS_METHOD_NAME = "setMapperClass";
	private final static String SETREDUCERCLASS_METHOD_NAME = "setReducerClass";
	
	private final static String[] MOCKED_METHOD_NAMES = {WAITFORCOMPLETION_METHOD_NAME, 
		SETMAPPERCLASS_METHOD_NAME, 
		SETREDUCERCLASS_METHOD_NAME};
	
	private Mapper<MapperKeyType,MapperValueType,ReducerKeyType,ReducerValueType> mapper;
	private Reducer<ReducerKeyType,ReducerValueType,OutputKeyType,OutputValueType> reducer;
	private KeyValuedStore<MapperKeyType,MapperValueType> inputData;
	private KeyValuedStore<OutputKeyType,OutputValueType> outputData;
	private KeyValuedStore<ReducerKeyType,ReducerValueType> mapperOutputBuffer;
	private MockedContextBuilder<ReducerKeyType,ReducerValueType> mockedContextBuilder;
	
	MockedHadoopWaitForCompletionSetMapperAndSetReducerJobMethods (Mapper<MapperKeyType,MapperValueType,ReducerKeyType,ReducerValueType> mapper,
			Reducer<ReducerKeyType,ReducerValueType,OutputKeyType,OutputValueType> reducer,
			KeyValuedStore<MapperKeyType,MapperValueType> inputData,
			KeyValuedStore<OutputKeyType,OutputValueType> outputData) 
	{
		super(MOCKED_METHOD_NAMES);	
		this.mapper = mapper;
		this.reducer = reducer;
		this.inputData = inputData;
		this.outputData = outputData;
		this.mapperOutputBuffer = new KeyValuedStore<ReducerKeyType,ReducerValueType>();
		this.mockedContextBuilder  = new MockedContextBuilder<ReducerKeyType,ReducerValueType>(mapperOutputBuffer);
	}

	@Override
	protected Object mockedMethodImplementation(InvocationOnMock invocation)  
	{	
		
		final String invocationMethodName = invocation.getMethod().getName();
		if (isAMockedEmptyVoidMethod(invocationMethodName)) return null;
		
		try {			
			
			mapperOutputBuffer.clear();
			
			executeMapper(mapper, inputData);
			
			KeyValuedStore<ReducerKeyType,List<ReducerValueType>> reducerInput = shuffleAndSortMapperOutput(mapperOutputBuffer);
			
			Iterator<KeyValuedBufferItem<ReducerKeyType,List<ReducerValueType>>> reducerInputIterator = reducerInput.iterator();
			
			while (reducerInputIterator.hasNext()) {
				KeyValuedBufferItem<ReducerKeyType, List<ReducerValueType>> reducerInputItem = reducerInputIterator.next();
						
				KeyValuedStore<ReducerKeyType,List<ReducerValueType>> currentReducerExecutionInputData = 
						new KeyValuedStore<ReducerKeyType, List<ReducerValueType>> ();			
				
				currentReducerExecutionInputData.append(reducerInputItem.getKey(), reducerInputItem.getValue());			
				
				executeReducer(reducer, currentReducerExecutionInputData, outputData);
			}
		}
		catch (IllegalAccessException | InvocationTargetException |NoSuchMethodException ex){
			throw new RuntimeException("Error in the mocked hadoop job object", ex);
		}
		
		return null;
	}
	
	private boolean isAMockedEmptyVoidMethod (String methodName)
	{
		return  isTheSetMapperClassMethod (methodName) || isTheSetReducerClassMethod (methodName);
	}
	
	private boolean isTheSetMapperClassMethod (String methodName)
	{
		return methodName.equals(SETMAPPERCLASS_METHOD_NAME);
	}
	
	private boolean isTheSetReducerClassMethod (String methodName)
	{
		return methodName.equals(SETREDUCERCLASS_METHOD_NAME);
	}				
	
	private void executeMapper(
			Mapper<MapperKeyType, MapperValueType, ReducerKeyType, ReducerValueType> mapper,
			KeyValuedStore<MapperKeyType, MapperValueType> inputData)			
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException 
	{
		Iterator<KeyValuedBufferItem<MapperKeyType,MapperValueType>> inputIterator = inputData.iterator();
		while (inputIterator.hasNext())
		{	
			KeyValuedBufferItem<MapperKeyType,MapperValueType> inputItem = inputIterator.next();
			final org.apache.hadoop.mapreduce.Mapper.Context mapperContext = mockedContextBuilder.buildMapperContext();
			executeMapMethod(mapper,
					inputItem.getKey(),
					inputItem.getValue(),
					mapperContext);						
		}
	}
	
	private void executeReducer(
			Reducer<ReducerKeyType,ReducerValueType,OutputKeyType,OutputValueType> reducer,
			KeyValuedStore<ReducerKeyType, List<ReducerValueType>> inputData,
			KeyValuedStore<OutputKeyType, OutputValueType> outputData) 
					throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
			
	{		
		MockedContextBuilder<OutputKeyType,OutputValueType> reducerContextBuilder 
			= new MockedContextBuilder<OutputKeyType,OutputValueType>(outputData);	
		org.apache.hadoop.mapreduce.Reducer.Context reducerContext = reducerContextBuilder.buildReducerContext();
		
		Iterator<KeyValuedBufferItem<ReducerKeyType, List<ReducerValueType>>> inputIterator = inputData.iterator();
		while (inputIterator.hasNext()) {
			KeyValuedBufferItem<ReducerKeyType, List<ReducerValueType>> inputItem = inputIterator.next();
			ReducerKeyType key = inputItem.getKey();
			List<ReducerValueType> values = inputItem.getValue();
			
			executeReducerMethod(reducer, key, values, reducerContext);
		}			
	}
	
	private KeyValuedStore<ReducerKeyType,List<ReducerValueType>> shuffleAndSortMapperOutput(
			KeyValuedStore<ReducerKeyType,ReducerValueType> mapperOutput)
	{
		KeyValuedStore<ReducerKeyType,List<ReducerValueType>> reducerInput 
			= new KeyValuedStore<ReducerKeyType,List<ReducerValueType>>();
		
		Iterator<ReducerKeyType> keyIterator = mapperOutput.keyIterator();
		List<ReducerKeyType> keysList = (List<ReducerKeyType>) sortIteratorOutput(keyIterator);
		
		for (ReducerKeyType currentKey : keysList) {
						
			Iterator<ReducerValueType> currentKeyValuesIterator = mapperOutput.filterOutputByKey(currentKey);
			List<ReducerValueType> currentKeyValues = (List<ReducerValueType>) sortIteratorOutput(currentKeyValuesIterator);
			
			reducerInput.append(currentKey, currentKeyValues);
						
		}				
		
		return reducerInput;
	}
		
	
	private List<Comparable> sortIteratorOutput(Iterator iterator)
	{
		List<Comparable> sortableList = iteratorToList(iterator);
		Collections.sort(sortableList);
		
		return sortableList;
	}
	
	private List<Comparable> iteratorToList(Iterator iterator)
	{		
		List<Comparable> list = new ArrayList<Comparable> ();
		
		while (iterator.hasNext()) {
			list.add((Comparable) iterator.next());
		}
		
		return list;
	}
	
	private Object executeMapMethod(Mapper<MapperKeyType,MapperValueType,ReducerKeyType,ReducerValueType> mapper,
			MapperKeyType key,
			MapperValueType value,
			org.apache.hadoop.mapreduce.Mapper.Context context) 
	throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
	{
		// Note: Is executed by reflection because of the map() method is protected in the base common Mapper class 
		Class mapperClass = mapper.getClass();
						
		Method method = findTheMostSpecificMethod(mapperClass, "map");		
		
		return method.invoke (mapper, key, value, context);
	}
	
	private Object executeReducerMethod(Reducer<ReducerKeyType,ReducerValueType,OutputKeyType,OutputValueType> reducer,
			ReducerKeyType key,
			List<ReducerValueType> values,
			org.apache.hadoop.mapreduce.Reducer.Context context) 
	throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
	{
		// Note: Is executed by reflection because of the map() method is protected in the base common Mapper class 
		Class mapperClass = reducer.getClass();
						
		Method method = findTheMostSpecificMethod(mapperClass, "reduce");		
		
		return method.invoke (reducer, key, values, context);
	}
	
	private Method findTheMostSpecificMethod(Class classToSearchIn, String searchedMethodName)
	{
		Method mostSpecificFoundMethod = null;
		
		Method[] allMethods = classToSearchIn.getMethods();
		for (Method currentMethod : allMethods) {
			
			final boolean theMethodNameDoesNotMatch = !currentMethod.getName().equals(searchedMethodName);
			if (theMethodNameDoesNotMatch) continue;
			
			mostSpecificFoundMethod = isMoreSpecificMethodThan(currentMethod, mostSpecificFoundMethod) ? currentMethod : mostSpecificFoundMethod;												
		}
		
		return mostSpecificFoundMethod;
	}
	
	private boolean isMoreSpecificMethodThan(Method currentMethod, Method previousMethod) 
	{	
		final boolean firstMethodFound = (previousMethod == null);
		if (firstMethodFound) return true;
						
		Class[] currentMethodParameters = currentMethod.getParameterTypes();
		final boolean currentMethodHasNotParameters = (currentMethodParameters == null);
		
		if (currentMethodHasNotParameters) return false;
																
		Class currentMethodfirstParameterClass = currentMethodParameters[0];
				
		final String currentMethodFirstParameterClassName = currentMethodfirstParameterClass.getName();
		final boolean currentMethodMoreSpecificThanThePreviousOne = !currentMethodFirstParameterClassName.equals("java.lang.Object");											
					
		return currentMethodMoreSpecificThanThePreviousOne;
	}	
}
