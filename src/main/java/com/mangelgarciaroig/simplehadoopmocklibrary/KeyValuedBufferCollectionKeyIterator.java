package com.mangelgarciaroig.simplehadoopmocklibrary;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class KeyValuedBufferCollectionKeyIterator <KeyType,ValueType> implements Iterator<KeyType> 
{	
	private List<KeyValuedBufferItem<KeyType,ValueType>> buffer;
	private int currentPosition = -1;
	private HashMap<KeyType,KeyType> discoveredKeys = new HashMap<KeyType,KeyType>();
	
	KeyValuedBufferCollectionKeyIterator(List<KeyValuedBufferItem<KeyType,ValueType>> buffer)
	{
		this.buffer = buffer;				
	}	

	@Override
	public boolean hasNext() 
	{
		int nextPosition = nextElementPosition();
		
		if (nextPosition > currentPosition){
			return true;
		}
		
		return false;
	}

	@Override
	public KeyType next() 
	{
		final boolean noMoreValuesAvailable = !hasNext();
		if (noMoreValuesAvailable) return null;
		
		final int nextPosition = nextElementPosition();
		KeyType next = buffer.get(nextPosition).getKey();					
		
		markKeyAsDiscovered(next);
		currentPosition = nextPosition;		
		
		return next;
	}

	@Override
	public void remove() 
	{		
		throw new UnsupportedOperationException(this.getClass().getName() + ".remove not supported");
	}
	
	private int nextElementPosition()
	{													
		for (int currentIndex = currentPosition + 1; currentIndex < buffer.size(); currentIndex++){
			KeyValuedBufferItem<KeyType,ValueType> currentBufferItem = buffer.get(currentIndex);
			
			final boolean aNewKeyValueWasFound = !KeyWasPreviouslyDiscovered(currentBufferItem.getKey());
			if ( aNewKeyValueWasFound ) return currentIndex;
		}
		
		return -1;
	}
	
	private boolean KeyWasPreviouslyDiscovered(KeyType key)
	{
		return discoveredKeys.containsKey(key);
	}	
	
	private void markKeyAsDiscovered(KeyType key)
	{
		discoveredKeys.put(key, key);
	}
}
