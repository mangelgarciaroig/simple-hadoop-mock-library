package com.mangelgarciaroig.simplehadoopmocklibrary;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class KeyValuedStore<KeyType,ValueType> 
{
	List<KeyValuedBufferItem<KeyType,ValueType>> bufferedOutputContent = new ArrayList<KeyValuedBufferItem<KeyType,ValueType>>();
	
	public void append(KeyType key, ValueType value)
	{
		KeyValuedBufferItem<KeyType,ValueType> newItem = new KeyValuedBufferItem<KeyType,ValueType>(key, value);
		append(newItem);
	}
	
	public void append(KeyValuedBufferItem<KeyType,ValueType> newOutput)
	{
		bufferedOutputContent.add(newOutput);
	}
	
	public Iterator<KeyType> keyIterator()
	{	
		return new  KeyValuedBufferCollectionKeyIterator <KeyType,ValueType>(bufferedOutputContent);
	}
	
	public Iterator<KeyValuedBufferItem<KeyType,ValueType>> iterator()
	{
		return bufferedOutputContent.iterator();
	}
	
	public Iterator<ValueType> filterOutputByKey(KeyType searchedKey)
	{
		return new FilteredMockedOutputBufferIterator<KeyType,ValueType>(bufferedOutputContent, searchedKey);
	}
	
	public int size()
	{
		return bufferedOutputContent.size();
	}
	
	public int countElementsByKey(KeyType keyToCompare)
	{
		int elementByKeyCount = 0;
		
		for (KeyValuedBufferItem<KeyType,ValueType> outputItem : bufferedOutputContent){
			elementByKeyCount+= outputItem.hasTheSameKeyOf(keyToCompare) ? 1 : 0;
		}
		
		return elementByKeyCount;
	}
	
	public void clear()
	{
		bufferedOutputContent.clear();
	}
	
	@Override
	public String toString()
	{
		StringBuffer str = new StringBuffer("{\n");		
		
		for (KeyValuedBufferItem<KeyType,ValueType> currentMapperOutput : bufferedOutputContent){
			str.append(currentMapperOutput.toString());
			str.append("\n");
		}
		
		str.append("}");
		
		return str.toString();
	}
	
	@Override
	public boolean equals(Object anotherObject)
	{
		if (anotherObject == null) return false;
		
		final boolean weDontBelongToTheSameClass = !anotherObject.getClass().equals(this.getClass());
		if (weDontBelongToTheSameClass) return false;
						
		final KeyValuedStore<KeyType,ValueType> theOtherOutputStore = (KeyValuedStore<KeyType,ValueType>) anotherObject;
		
		final int mySize = this.size();
		final int theOtherStoreSize = theOtherOutputStore.size();
		
		final boolean theSizesAreDistinct = (mySize != theOtherStoreSize);		
		if (theSizesAreDistinct) return false;
						
		Iterator<KeyValuedBufferItem<KeyType,ValueType>> myOutputIterator = this.iterator();
		Iterator<KeyValuedBufferItem<KeyType,ValueType>> theOtherStoreOutputIterator = theOtherOutputStore.iterator();
		
		if (!theOutputItemsAreEqual(myOutputIterator, theOtherStoreOutputIterator)) return false;
									
		return true;
	}
	
	private boolean theOutputItemsAreEqual (Iterator<KeyValuedBufferItem<KeyType,ValueType>> myOutputIterator, 
			Iterator<KeyValuedBufferItem<KeyType,ValueType>> theOtherStoreOutputIterator)
	{
		while (myOutputIterator.hasNext()) {
			final boolean theOtherHasReachedTheEnd = !theOtherStoreOutputIterator.hasNext();
			if (theOtherHasReachedTheEnd) return false;
			
			final KeyValuedBufferItem<KeyType,ValueType> myCurrentOutputItem = myOutputIterator.next();
			final KeyValuedBufferItem<KeyType,ValueType> theOtherStoreCurrentOutputItem = theOtherStoreOutputIterator.next();
			
			final boolean theOutputItemsAreDistinct = !myCurrentOutputItem.equals(theOtherStoreCurrentOutputItem);
			if (theOutputItemsAreDistinct) return false;
		}
		
		final boolean theOtherStoreIsEmpty= !theOtherStoreOutputIterator.hasNext();
		
		final boolean theStoresAreEqual = theOtherStoreIsEmpty;
							
		return theStoresAreEqual;
	}
}