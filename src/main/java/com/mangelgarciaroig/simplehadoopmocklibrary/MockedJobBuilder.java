package com.mangelgarciaroig.simplehadoopmocklibrary;

import static org.mockito.Mockito.mock;

import java.io.IOException;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

public class MockedJobBuilder<MapperKeyType,MapperValueType,ReducerKeyType,ReducerValueType,OutputKeyType,OutputValueType>
{		
	public Job build(Mapper<MapperKeyType,MapperValueType,ReducerKeyType,ReducerValueType> mapper,
			Reducer<ReducerKeyType,ReducerValueType,OutputKeyType,OutputValueType> reducer,
			KeyValuedStore<MapperKeyType,MapperValueType> inputData,
			KeyValuedStore<OutputKeyType,OutputValueType> outputData) throws IOException 	
	{		
		MockedHadoopWaitForCompletionSetMapperAndSetReducerJobMethods<MapperKeyType,MapperValueType,ReducerKeyType,ReducerValueType,OutputKeyType,OutputValueType> 
		jobWaitForCompletionSetMapperAndSetReducerMock = 
				new MockedHadoopWaitForCompletionSetMapperAndSetReducerJobMethods <MapperKeyType,MapperValueType,ReducerKeyType,ReducerValueType,OutputKeyType,OutputValueType>(
						mapper, 
						reducer, 
						inputData, 
						outputData);
		
		Job job = (Job) mock(Job.class, jobWaitForCompletionSetMapperAndSetReducerMock);
				
		job.setMapperClass(mapper.getClass());
		job.setReducerClass(reducer.getClass());
		
		return job;
	}
	
}