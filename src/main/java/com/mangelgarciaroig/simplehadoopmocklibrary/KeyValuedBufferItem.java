package com.mangelgarciaroig.simplehadoopmocklibrary;

public class KeyValuedBufferItem<KeyType,ValueType> 
{
	private KeyType key;
	
	private ValueType value;
	
	public KeyValuedBufferItem(KeyType key, ValueType value)
	{
		this.key = key;
		this.value = value;
	}

	public KeyType getKey() {
		return key;
	}

	public ValueType getValue() {
		return value;
	}	
	
	public boolean hasTheSameKeyOf(Object keyToCompare)
	{		
		boolean compatibleTypes = key.equals(keyToCompare);
		
		return compatibleTypes;
	}
	
	@Override
	public String toString()
	{
		StringBuffer str = new StringBuffer();
		
		str.append(key.toString());
		str.append(" : ");
		str.append(value.toString());
		
		return str.toString();
	}
	
	@Override
	public boolean equals(Object anotherObject)
	{
		if (anotherObject == null) return false;
		
		final boolean weDontBelongToTheSameClass = !anotherObject.getClass().equals(this.getClass());
		if (weDontBelongToTheSameClass) return false;
		
		final KeyValuedBufferItem<KeyType,ValueType> theOtherOutputItem = (KeyValuedBufferItem<KeyType,ValueType>) anotherObject;
		
		final boolean keysAreEqual = this.key.equals(theOtherOutputItem.key);
		final boolean valuesAreEqual = this.value.equals(theOtherOutputItem.value);
		
		final boolean deeperMemberEquality = keysAreEqual && valuesAreEqual;
		
		return deeperMemberEquality;
	}
}
