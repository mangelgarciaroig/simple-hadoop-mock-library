package com.mangelgarciaroig.simplehadoopmocklibrary;

import java.io.IOException;
import org.mockito.invocation.InvocationOnMock;


import com.rits.cloning.Cloner;

public class MockedHadoopContextWriteMethod<KeyType, ValueType> extends MockedHadoopGenericMethod
{
	private final static String MOCKED_METHOD_NAME = "write";
	private KeyValuedStore<KeyType, ValueType> mapperOutputBuffer;
	private Cloner cloner;
	
	public MockedHadoopContextWriteMethod(KeyValuedStore<KeyType, ValueType> mapperOutputBuffer, Cloner cloner)
	{
		super(MOCKED_METHOD_NAME);
		
		this.mapperOutputBuffer = mapperOutputBuffer;
		this.cloner = cloner;
	}	
	
	@Override
	protected Object mockedMethodImplementation (InvocationOnMock invocation)  	
	{				
		try {
			Object[] invocationArgs = invocation.getArguments();		
		
			KeyType key = (KeyType) clone(invocationArgs[0]);
			ValueType value = (ValueType) clone(invocationArgs[1]);								
						
			KeyValuedBufferItem<KeyType, ValueType> newOutput = new KeyValuedBufferItem<KeyType, ValueType>(key, value);
			mapperOutputBuffer.append(newOutput);
		}
		catch (IOException | ClassNotFoundException ex){
			throw new RuntimeException("Error invoking mocked method " + MOCKED_METHOD_NAME, ex);
		}
				
		return null;
	}		
	
	private Object clone(Object toBeCloned) throws IOException, ClassNotFoundException
	{						
		return cloner.deepClone(toBeCloned);
	}

		
}
