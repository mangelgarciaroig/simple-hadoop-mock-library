package com.mangelgarciaroig.simplehadoopmocklibrary;

public class KeyValuedStoreBuilder<KeyType, ValueType> 
{
	private KeyValuedStore<KeyType, ValueType> store;
	
	public KeyValuedStoreBuilder()
	{
		clear();
	}
	
	public void addItem(KeyType key, ValueType value)
	{
		KeyValuedBufferItem<KeyType, ValueType> newOutputItem = new KeyValuedBufferItem<KeyType, ValueType>(key, value);
		addItem(newOutputItem);
	}
	
	public void addItem (KeyValuedBufferItem<KeyType, ValueType> newOutputItem)
	{
		store.append(newOutputItem);
	}
	
	public KeyValuedStore<KeyType, ValueType> build()
	{
		KeyValuedStore<KeyType, ValueType> buildedStore = store;
		clear();
		
		return buildedStore;
	}
	
	public void clear()
	{
		store = new KeyValuedStore<KeyType, ValueType>();
	}
}
