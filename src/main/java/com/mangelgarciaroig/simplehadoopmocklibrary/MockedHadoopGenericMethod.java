package com.mangelgarciaroig.simplehadoopmocklibrary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

abstract class MockedHadoopGenericMethod implements Answer
{	
	private List<String> clonedMethodNames;
		
	protected MockedHadoopGenericMethod(String clonedMethodName)
	{	
		this(new String[] {clonedMethodName });
	}
	
	protected MockedHadoopGenericMethod(String... clonedMethodNames)
	{
		this.clonedMethodNames = new ArrayList<String>(Arrays.asList(clonedMethodNames)) ;		
	}
	
	/**
	 * Override to provide the specific method code to mock
	 * @param invocation
	 * @return
	 */
	protected abstract Object mockedMethodImplementation(InvocationOnMock invocation);
	
	@Override
	public Object answer(InvocationOnMock invocation) throws Throwable 
	{
		final String invocationMethodName = invocation.getMethod().getName();		

		if (shouldBeInvokedNativelly(invocationMethodName)) return invokeOriginalContextMethod(invocation);		
		
		Object methodResponse = mockedMethodImplementation(invocation);
		
		return methodResponse;
	}
	
	private Object invokeOriginalContextMethod (InvocationOnMock invocation)
			throws Throwable 
	{
		return invocation.callRealMethod();
	}
	
	private boolean shouldBeInvokedNativelly(String methodName)
	{		
		final boolean isNotAMockedMethod = !clonedMethodNames.contains(methodName);
		
		return isNotAMockedMethod;
	}
	
}
