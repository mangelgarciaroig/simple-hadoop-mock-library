# README #

This library is founded with the goal of providing an easy way of develop unit tests against Hadoop map/reduce jobs, with no dependencies on the execution platform (no hadoop installation is needed in order to run the tests). 

Another excelent libraries exists, like MRUnit, but they need a full hadoop installation to work properly, and it can be sometimes problematic (big teams, continous integration platforms). 

### How do I get set up? ###

You need maven in order to build the library. Use maven workflow as usual.

### Usage ###

#### Test a mapper ####
Full example at **com.mangelgarciaroig.simplehadoopmocklibrary.WordCountMapperTest**
        
```
#!java

@Test
	public void testMapperSimpified()
	{
		final String aWord = "dog";
		final String anotherWord = "cat";
		final String lineInputToBeProcessed = aWord + " " + anotherWord + " " + aWord;
		final int lineNumber = 0;
			
		mapper.map(new LongWritable(lineNumber), new Text(lineInputToBeProcessed), mockedContext);
		
		KeyValuedStoreBuilder<Text, LongWritable> expectedStoreBuilder = new KeyValuedStoreBuilder<Text, LongWritable>();
		expectedStoreBuilder.addItem(new Text(aWord), new LongWritable(1));
		expectedStoreBuilder.addItem(new Text(anotherWord), new LongWritable(1));
		expectedStoreBuilder.addItem(new Text(aWord), new LongWritable(1));
		KeyValuedStore<Text, LongWritable> expectedOutput =  expectedStoreBuilder.build();
		
		assertEquals("The word count mapper output was not the expected", expectedOutput, mockedOutputBuffer);
	}
```

#### Test a reducer ####
Full example at **com.mangelgarciaroig.simplehadoopmocklibrary.WordCountReducerTest**

        
```
#!java

@Before
	public void setUp()
	{		
		mockedOutputBuffer = new KeyValuedStore<Text, LongWritable>();
		MockedContextBuilder<Text, LongWritable> builder = new MockedContextBuilder<Text, LongWritable>(mockedOutputBuffer);		
		
		mockedContext = builder.buildReducerContext();
		reducer = new WordCountReducer();
	}
       @Test
	public void testReducer()
	{	
		final String aWord = "perro";	
		final long expectedWordCount = 3;
		
		List<LongWritable> valuesToBeReduced = new ArrayList<LongWritable>();
		valuesToBeReduced.add(new LongWritable(1));		
		valuesToBeReduced.add(new LongWritable(2));
		
		reducer.reduce(new Text(aWord), valuesToBeReduced, mockedContext);
		
		final int expectedReducedOutputSize = 1;
		final int obtainedReducedOutputSize = mockedOutputBuffer.size();
		
		KeyValuedStoreBuilder<Text, LongWritable> expectedStoreBuilder = new KeyValuedStoreBuilder<Text, LongWritable>();
		expectedStoreBuilder.addItem(new Text(aWord), new LongWritable(expectedWordCount));		
		KeyValuedStore<Text, LongWritable> expectedOutput =  expectedStoreBuilder.build();
				
		assertEquals("Unexpected word count reducer output size", expectedReducedOutputSize, obtainedReducedOutputSize);									
		assertEquals("Unexpected word count reducer output content", expectedOutput, mockedOutputBuffer);
	}

```
#### Test a full map/reduce job  ####

Full example at **com.mangelgarciaroig.simplehadoopmocklibrary.WordCountJobTest**

       
```
#!java

@Test
	public void testJob() 
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException, ClassNotFoundException, InterruptedException
	{
		final String word1 = "aa";
		final String word2 = "cat";
		final String word3 = "dog";
		final long frequencyWord1 = 1;
		final long frequencyWord2 = 2;
		final long frequencyWord3 = 3;
		
		MockedJobBuilder<LongWritable, Text, Text, LongWritable, Text, LongWritable> mockedJobBuilder 
			= new MockedJobBuilder<LongWritable, Text, Text, LongWritable, Text, LongWritable>();					
		
		WordCountMapper mapper = new WordCountMapper();
		WordCountReducer reducer = new WordCountReducer();
		
		final String firstInputLine = String.format("%s %s %s", word3, word2, word3);
		final String secondInputLine = String.format("%s %s %s", word1, word2, word3);
		
		KeyValuedStore<LongWritable, Text> input = buildJobInput(firstInputLine, secondInputLine);		
		KeyValuedStore<Text, LongWritable> output = new KeyValuedStore<Text, LongWritable>();
				
		Job job = mockedJobBuilder.build(mapper, reducer, input, output);						
		job.waitForCompletion(true);
		
		KeyValuedStore<Text, LongWritable> expectedOutput = new KeyValuedStore<Text, LongWritable>();
		expectedOutput.append(new Text(word1), new LongWritable(frequencyWord1));
		expectedOutput.append(new Text(word2), new LongWritable(frequencyWord2));
		expectedOutput.append(new Text(word3), new LongWritable(frequencyWord3));
		
		assertEquals("Unexpected job output", expectedOutput, output);			
	}
	
	private KeyValuedStore<LongWritable, Text> buildJobInput (String... inputLines){
		KeyValuedStore<LongWritable, Text> input = new KeyValuedStore<LongWritable, Text>();
		
		long lineNumber = 0;
		for (String currentInputLine : inputLines){
			input.append(new LongWritable(lineNumber++), new Text(currentInputLine));
		}
		
		return input;
	}
```
